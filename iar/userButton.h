#ifndef _USERBUTTON_
#define _USERBUTTON_
#include <stdlib.h>
#include "gpio.h"

class UserButton
{
private:
  uint8_t PIN1 = 3;//PB3;
  uint8_t PIN2 = 5;//PB5;
  GPIO* port;
  
public:
  UserButton(): port(new GPIO('B'))
  {
    port->directionPin(PIN1, 0);
    port->directionPin(PIN2, 0);
  }
  char getButton()
  {
    //sleep(20);
    if(port->readPin(PIN1))
    {
      if(port->readPin(PIN2))
      {
        return'X';
      }
      else
      {
        return 'B';
      }
    }
    else
    {
      if(port->readPin(PIN2))
      {
        return'F';
      }
      else
      {
        return 0;
      }
    }
  }
};
#endif