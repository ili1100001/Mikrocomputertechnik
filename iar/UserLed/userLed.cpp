#include "../gpio.h"
#include "userLed.h"

userLed* userLed::_exemplar = 0;

userLed* userLed::Exemplar()
{
  if(_exemplar == 0)
  {
    _exemplar = new userLed();
  }
  return _exemplar;
}

void userLed::setStateLedRed(bool state)
{
  GPIOA->setPin(8, state);
}

void userLed::setStateLedYellow(bool state)
{
  GPIOA->setPin(9, state);
}


void userLed::setStateLedGreen(bool state)
{
  GPIOA->setPin(10, state);
}


void userLed::blinckLedYellow()
{
  GPIOA->invertPin(9);
}


void userLed::setStateLeds(int state)
{
  setStateLedRed(state & 0x01);
  setStateLedYellow(state & 0x02);
  setStateLedGreen(state & 0x04);
}
