#include "../gpio.h"

#ifndef USERLED_H
#define USERLED_H

class userLed
{
private:
  static userLed* _exemplar;
  GPIO* GPIOA;
  
protected:
  userLed(): GPIOA(new GPIO('A'))
  {
    GPIOA->directionPin(8, 1);
    GPIOA->directionPin(9, 1);
    GPIOA->directionPin(10, 1);
  }
  
public:
  static userLed* Exemplar();
  void setStateLedRed(bool state);
  void setStateLedYellow(bool state);
  void setStateLedGreen(bool state);
  void blinckLedYellow();
  void setStateLeds(int state);
};

#endif