#ifndef H_SPI
#define H_SPI

#define RCC_APB2ENR (*(volatile uint32_t*)0x40023844)

class SPI
{
protected:
  volatile uint16_t& cr1;
  volatile uint16_t& cr2;
  volatile uint16_t& sr;
  volatile uint16_t& dr;
  volatile uint16_t& crcpr;
  volatile uint16_t& rxcrcr;
  volatile uint16_t& txcrcr;
  volatile uint16_t& i2scfgr;
  volatile uint16_t& i2spr;  
  
public:
  //Adressen Seite 53
  //default is SPI1
  SPI(uint32_t baseAddr = 0x40013000) : cr1(*(volatile uint16_t*)baseAddr),
    cr2(*(volatile uint16_t*)(&cr1 + 2)),
    sr(*(volatile uint16_t*)(&cr1 + 2*2)),
    dr(*(volatile uint16_t*)(&cr1 + 2*3)),
    crcpr(*(volatile uint16_t*)(&cr1 + 2*4)),
    rxcrcr(*(volatile uint16_t*)(&cr1 + 2*5)),
    txcrcr(*(volatile uint16_t*)(&cr1 + 2*6)),
    i2scfgr(*(volatile uint16_t*)(&cr1 + 2*7)),
    i2spr(*(volatile uint16_t*)(&cr1 + 2*8))
  {
    //start SPI1 clock s.154
    RCC_APB2ENR |= (1 << 12);
    
    //this is the constructor
    cr1 = (0x1 << 9)/*SSM*/ | (0x0 << 11)/*8-bit data*/ | (0x0 << 7)/*MSB first*/ | (0x3 << 3)/*set baudrate*/ | (0x0 << 1)/*polarity*/ | (0x0 << 0)/*clock phase*/;
    cr2 = (0x0 << 4)/*SPI in Motorola mode*/ | (0x1 << 2)/*enable chip select output*/;
    
    //start
    cr1 |= (0x1 << 6)/*enable*/ | (0x1 << 2)/*set master mode*/;
  }
  
   void send(uint8_t data)
   {
     while( (sr & (1 << 1)) ){};
     dr = (uint16_t)data;
   }
};

#endif