
#ifndef _Green_STATE_
#define _Green_STATE_

#include "LedState.h"

class GreenState : public LedState
{

public:

    LedState* getNextState(){
        userLed::Exemplar()->setStateLedRed(false);
        userLed::Exemplar()->setStateLedYellow(false);
        userLed::Exemplar()->setStateLedGreen(true);
        return LedState::getNextState();
    }
};


#endif

