#ifndef _LED_STATE_
#define _LED_STATE_

#include "../UserLed/userLed.h"

class LedState
{


protected:
    LedState* pNext;

public:
  LedState(): pNext(0){};

    void setNextState(LedState* next){
        pNext = next;
    }

    LedState* getNextState(){
        return pNext;
    }
};

#endif

