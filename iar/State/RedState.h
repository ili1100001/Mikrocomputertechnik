
#ifndef _RED_STATE_
#define _RED_STATE_

#include "LedState.h"

class RedState : public LedState
{

public:


    LedState* getNextState(){
        userLed::Exemplar()->setStateLedRed(true);
        userLed::Exemplar()->setStateLedYellow(false);
        userLed::Exemplar()->setStateLedGreen(false);
        return LedState::getNextState();
    }
};



#endif

