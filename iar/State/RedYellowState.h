
#ifndef _RED_YELLOW_STATE_
#define _RED_YELLOW_STATE_

#include "LedState.h"

class RedYellowState : public LedState
{

public:

    LedState* getNextState(){
        userLed::Exemplar()->setStateLedRed(true);
        userLed::Exemplar()->setStateLedYellow(true);
        userLed::Exemplar()->setStateLedGreen(false);
        return LedState::getNextState();
    }
};


#endif

