#ifndef _MACHINE_STATE_
#define _MACHINE_STATE_

#include <string>
using namespace std;

    enum Event { ButtonB, ButtonF, ButtonX };

class MachineState
{


public:

    virtual MachineState* handleEvent(Event e) = 0;
    virtual void run() = 0;
    virtual string getState() = 0;

};


#endif
