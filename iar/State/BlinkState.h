#ifndef _BLINK_STATE_
#define _BLINK_STATE_

#include "MachineState.h"
#include "RunState.h"

class BlinkState : public MachineState
{

public:

    MachineState* handleEvent(Event e);
    void run();
    string getState() {
        return "BlinkState";
    };

};


#endif
