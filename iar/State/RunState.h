#ifndef _RUN_STATE_
#define _RUN_STATE_

#include "MachineState.h"
#include "BlinkState.h"

#include <iostream>

#include "RedState.h"
#include "RedYellowState.h"
#include "GreenState.h"
#include "YellowState.h"

#include "LedState.h"


class RunState : public MachineState
{

private:
RedState* redState;
RedYellowState* redYellowState;
GreenState* greenState;
YellowState* yellowState;

LedState* ledState;


public:

    RunState() : redState( new RedState() ), redYellowState( new RedYellowState() ),
                greenState( new GreenState() ), yellowState( new YellowState() ), ledState( redState ){


        redState->setNextState(redYellowState);
        redYellowState->setNextState(greenState);
        greenState->setNextState(yellowState);
        yellowState->setNextState(redState);

    };


    MachineState* handleEvent(Event e);;

    void run();

    string getState() {
        return "RunState";
    };

};

#endif
