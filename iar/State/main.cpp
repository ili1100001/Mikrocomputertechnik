#include <iostream>
#include "Context.h"

using namespace std;

int main()
{
    cout << "Hello world!" << endl;

    Context context;
    context.run();

    return 0;
}
