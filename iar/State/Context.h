#ifndef _CONTEXT_
#define _CONTEXT_

#define DEBUG

#include "MachineState.h"
#include "BlinkState.h"
#include <iostream>

#include "../userButton.h"

class Context
{
protected:
    MachineState* machineState;
    UserButton* button;

public:
    Context(): machineState(new BlinkState()){
    }
    void run();

};


#endif


