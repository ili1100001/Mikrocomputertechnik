

#ifndef _YELLOW_STATE_
#define _YELLOW_STATE_

#include "LedState.h"

class YellowState : public LedState
{

public:

    LedState* getNextState(){
        userLed::Exemplar()->setStateLedRed(false);
        userLed::Exemplar()->setStateLedYellow(true);
        userLed::Exemplar()->setStateLedGreen(false);
        return LedState::getNextState();
    }
};


#endif

