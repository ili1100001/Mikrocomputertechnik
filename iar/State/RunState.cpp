#include "RunState.h"

MachineState* RunState::handleEvent(Event e){

    switch(e){
    case ButtonB:
        return new BlinkState();
    case ButtonF:
        ledState = ledState->getNextState();
        return this;
    default:
        return 0;
    }

}

void RunState::run(){

}
