#ifndef H_GPIO
#define H_GPIO

//Datenblatt S200

#include <stdint.h>

#define RCC_AHB1ENR (*(volatile uint32_t*)0x40023830)

class GPIO
{                                           
protected:
//Registeradressen f�r Port A
  volatile uint32_t& moder; /*GPIO port A mode register*/
  volatile uint32_t& otyper; /*GPIO port A output type register*/
  volatile uint32_t& ospeedr; /*GPIO port A output speed register*/
  volatile uint32_t& pupdr; /*GPIO port A pull-up/pull-down register*/
  volatile uint32_t& idr; /*GPIO port A input data register */
  volatile uint32_t& odr; /*GPIO port A output data register */
  volatile uint16_t& bsrrl; /*GPIO port A bit set/reset low register*/
  volatile uint16_t& bsrrh; /*GPIO port A bit set/reset highe register*/
  
public:
  GPIO(char portName) : moder(*(volatile uint32_t*)(0x40020000ul + ((uint32_t)(portName - 'A') * 0x00000400ul))),
    otyper(*(volatile uint32_t*)(&moder + 1)),
    ospeedr(*(volatile uint32_t*)(&moder + 2)),
    pupdr(*(volatile uint32_t*)(&moder + 3)),
    idr(*(volatile uint32_t*)(&moder + 4)),
    odr(*(volatile uint32_t*)(&moder + 5)),
    bsrrl(*(volatile uint16_t*)(&moder + 6)),
    bsrrh(*(volatile uint16_t*)(&bsrrl + 1))
  {
      /*Initialize Port x as Output*/
      RCC_AHB1ENR |= (1ul << ( portName - 'A' )); /* Enable GPIOx clock */
  }
  
  /**
  @param uint8_t pinNr
  @param bool direction 0: input, 1: output
  */
  void directionPin(uint8_t pinNr, bool direction)
  {
    moder = (moder & ~(3ul << (pinNr << 1ul)))/*kill all bits except pinNr*/ | ((uint32_t)direction << (pinNr << 1ul))/*set new value*/;
  }
  
  void setPin(uint8_t pinNr, bool high)
  {
    odr = (odr & ~(1 << pinNr)) | ((uint32_t)high << pinNr);
  }
  
  void setPin(uint8_t pinNr)
  {
    bsrrl = (1ul << pinNr);
  }
  
  void clearPin(uint8_t pinNr)
  {
    bsrrh = (1ul << pinNr);
  }
  
  void invertPin(uint8_t pinNr)
  {
    odr ^= (1 << pinNr);
  }
  
  bool readPin(uint8_t pinNr) const
  {
    return (bool)(idr & (1 << pinNr));
  }
  
  uint32_t readPort() const
  {
    return idr;
  }
  
  void writePort(uint32_t value)
  {
    odr = value;
  }
};

#endif