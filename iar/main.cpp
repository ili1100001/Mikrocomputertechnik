#include <stdio.h>
#include "gpio.h"
#include "spi.h"
#include "UserLed/userLed.h"
#include "userButton.h"
#include <iostream>

#include "State/Context.h"

#include <stdlib.h>

using namespace std;

int main()
{
  Context* context= new Context();
  context->run();
  return 0;
}